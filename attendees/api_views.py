from django.http import JsonResponse

from .models import Attendee


def api_list_attendees(request, conference_id):
    try:
        attendees = Attendee.objects.filter(conference_id=conference_id)

        attendee_list = []

        for attendee in attendees:
            attendee_dict = {
                "name": attendee.name,
                "href": attendee.get_api_url(),
            }
            attendee_list.append(attendee_dict)

        response_data = {
            "attendees": attendee_list
        }

        return JsonResponse(response_data)

    except Attendee.DoesNotExist:
        return JsonResponse({})

def api_show_attendee(request, id):
    try:
        attendee = Attendee.objects.get(id=id)

        conference_dict = {
            "name": attendee.conference.name,
            "href": attendee.conference.get_api_url(),
        }

        response_data = {
            "email": attendee.email,
            "name": attendee.name,
            "company_name": attendee.company_name,
            "created": attendee.created,
            "conference": conference_dict,
        }

        return JsonResponse(response_data)

    except Attendee.DoesNotExist:
        return JsonResponse({})
