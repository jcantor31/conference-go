from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Presentation


def api_list_presentations(request, conference_id):
    try:
        presentations = Presentation.objects.filter(conference=conference_id)

        presentation_list = []

        for presentation in presentations:
            presentation_dict = {
                "title": presentation.title,
                "status": presentation.status.name,
                "href": presentation.get_api_url(),
            }
            presentation_list.append(presentation_dict)

        response_data = {
            "presentations": presentation_list
        }

        return JsonResponse(response_data)

    except Presentation.DoesNotExist:
        return JsonResponse({})


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]


def api_show_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )
